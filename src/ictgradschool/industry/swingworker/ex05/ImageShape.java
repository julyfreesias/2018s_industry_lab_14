package ictgradschool.industry.swingworker.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;  //static?
    private LoadingImageRectangle lI;

    /*While the image is still loading, the shape should render as a placeholder
    rectangle with the text “Loading...” centred on the shape.
    Once the image is loaded and scaled, it should be rendered on the shape as normal.
    When scaling an image using ​Image.getScaledInstance()​, the returned image will be scaled ​lazily​.

    That is, the scaling operation won’t actually be run until the first time the image is used.
    If the first time the image is used is when its drawn in the ​paint()​ method,
    this can cause unexpected stuttering. One way around this is to use the image in some way
    before returning it from the background thread. Anything will do, even just, for example,
    calling its getWidth(...)​ method.*/

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        super(x, y, deltaX, deltaY, width, height);
        new LoadingImageRectangle(width, height, new File(fileName).toURI().toURL()).execute();
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        super(x, y, deltaX, deltaY, width, height);
        new LoadingImageRectangle(width, height, uri.toURL()).execute();

    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) throws MalformedURLException {
        super(x, y, deltaX, deltaY, width, height);
        new LoadingImageRectangle(width, height, url).execute();

    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);




    }
    public class LoadingImageRectangle extends SwingWorker<Image,String> {

        private int width;
        private int height;
        private URL url;


        public LoadingImageRectangle(int width,int height,URL url){
            this.width=width;
            this.height=height;
            this.url=url;

        }


        @Override
        protected Image doInBackground() throws Exception {
            try {
                Image image = ImageIO.read(url);
                if (width == image.getWidth(null) && height == image.getHeight(null)) {
                    return image;
                } else {
                    Image newImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    newImage.getWidth(null);
                    return newImage;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return image;
        }

        @Override
        protected void done() {
            try {
                Image image = get();
                ImageShape.this.image = image;

            }catch (InterruptedException e){
                e.printStackTrace();
            }catch (ExecutionException e){
                e.printStackTrace();
            }


        }


    }



    @Override
    public void paint(Painter painter) {
        if (image==null) {
            painter.setColor(Color.lightGray);
            painter.fillRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.RED);
            painter.drawRect(fX, fY, fWidth, fHeight);
            painter.setColor(Color.RED);
            painter.drawCenteredText("Loading...", fX, fY, fWidth, fHeight);

        }else {
            painter.drawImage(this.image, fX, fY, fWidth, fHeight);
        }
    }
}
